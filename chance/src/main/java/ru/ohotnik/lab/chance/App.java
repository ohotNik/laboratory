package ru.ohotnik.lab.chance;

import java.security.SecureRandom;

/**
 * @author Aleksander Okhonchenko.
 * @since 16.06.17.
 */
public class App {

    // 0 - можно открыть. ничего нет.
    // 1 - приз
    // 2 - открыто
    // 3 - выбрано
    private static int[] vals = new int[100];
    private static final SecureRandom RANDOM = new SecureRandom();

    public static void main(String[] args) {
        int opened;
        int success = 0;
        int expCount = 1000;
        for (int i = 0; i < expCount; i++) {

            //заполнение призовой ячейки.
            vals = new int[100];
            opened = 0;
            int prize = RANDOM.nextInt(100);
            int selected = RANDOM.nextInt(100);
            vals[prize] = 1;
            vals[selected] = 3;

            while (100 - opened > 2) {
                for (int j = 0; j < 100; j++) {
                    if (100 - opened == 2)
                        break;
                    if (vals[j] != 0)
                        continue;
                    if (RANDOM.nextBoolean()) {
                        vals[j] = 2;
                        opened++;
                    }
                }
            }

            if (selected == prize)
                success++;

        }

        final double result = (float)success / expCount;
        System.out.println("Шанс угадать: " + result);
    }

}
